package com.example.appmenubuttom92.DataBase

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class AlumnosDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "sistema.db"
        private const val DATABASE_VERSION = 2
    }

    init {
        val dbFile = context.getDatabasePath(DATABASE_NAME)
        println("Database path: ${dbFile.absolutePath}")
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE ${DefinirDB.Alumnos.TABLA} (
                ${DefinirDB.Alumnos.ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                ${DefinirDB.Alumnos.MATRICULA} TEXT,
                ${DefinirDB.Alumnos.NOMBRE} TEXT,
                ${DefinirDB.Alumnos.DOMICILIO} TEXT,
                ${DefinirDB.Alumnos.ESPECIALIDAD} TEXT,
                ${DefinirDB.Alumnos.FOTO} TEXT
            )
        """)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${DefinirDB.Alumnos.TABLA}")
        onCreate(db)
    }
}
