package com.example.appmenubuttom92.DataBase

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf
import java.net.IDN

class dbAlumnos(private val context: Context) {

    private var dbHelper: AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirDB.Alumnos.ID,
        DefinirDB.Alumnos.MATRICULA,
        DefinirDB.Alumnos.NOMBRE,
        DefinirDB.Alumnos.DOMICILIO,
        DefinirDB.Alumnos.ESPECIALIDAD,
        DefinirDB.Alumnos.FOTO
    )

    fun openDataBase() {
        db = dbHelper.writableDatabase
    }

    fun InsertarAlumno(alumno: Alumno): Long {
        val valores = contentValuesOf().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        return db.insert(DefinirDB.Alumnos.TABLA, null, valores)
    }

    fun ActualizarAlumno(alumno: Alumno, id: Int): Int {
        val valores = contentValuesOf().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        return db.update(
            DefinirDB.Alumnos.TABLA, valores,
            "${DefinirDB.Alumnos.ID}= $id", null
        )
    }

    fun BorrarAlumno(matricula: String): Int {
        return db.delete(
            DefinirDB.Alumnos.TABLA,
            "${DefinirDB.Alumnos.MATRICULA}=?",
            arrayOf(matricula)
        )
    }

    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }

    fun getAlumno(id: Long): Alumno {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA, leerCampos, "${DefinirDB.Alumnos.ID} = ?",
            arrayOf(id.toString()), null, null, null
        )
        cursor.moveToFirst()
        val alumno = mostrarAlumnos(cursor)
        cursor.close()
        return alumno
    }

    fun BuscarAlumno(matricula: String): Alumno? {
        val db = dbHelper.readableDatabase
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA, leerCampos, "${DefinirDB.Alumnos.MATRICULA} = ?",
            arrayOf(matricula), null, null, null
        )
        return if (cursor.moveToFirst()) {
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            alumno
        } else {
            cursor.close()
            null
        }
    }

    fun leerTodos(): ArrayList<Alumno> {
        val cursor = db.query(DefinirDB.Alumnos.TABLA, leerCampos, null, null, null, null, null)
        val listaAlumno = ArrayList<Alumno>()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val alumno = mostrarAlumnos(cursor)
            listaAlumno.add(alumno)
            cursor.moveToNext()
        }
        cursor.close()
        return listaAlumno
    }

    fun close() {
        dbHelper.close()
    }
}

