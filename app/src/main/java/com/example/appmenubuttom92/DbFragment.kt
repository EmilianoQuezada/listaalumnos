package com.example.appmenubuttom92

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.appmenubuttom92.DataBase.Alumno
import com.example.appmenubuttom92.DataBase.dbAlumnos

class DbFragment : Fragment() {

    private lateinit var btnGuardar: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var txturl: EditText
    private lateinit var db: dbAlumnos

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnLimpiar = view.findViewById(R.id.btnLimpiar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txturl = view.findViewById(R.id.txturl)

        btnGuardar.setOnClickListener {
            if (validarCampos()) {
                val matricula = txtMatricula.text.toString()
                val alumno = Alumno(
                    nombre = txtNombre.text.toString(),
                    matricula = matricula,
                    domicilio = txtDomicilio.text.toString(),
                    especialidad = txtEspecialidad.text.toString(),
                    foto = txturl.text.toString()
                )
                db.openDataBase()
                val alumnoExistente = db.BuscarAlumno(matricula)
                try {
                    if (alumnoExistente != null) {
                        val result = db.ActualizarAlumno(alumno, alumnoExistente.id)
                        if (result > 0) {
                            Toast.makeText(requireContext(), "Alumno actualizado exitosamente", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                        } else {
                            Toast.makeText(requireContext(), "Error al actualizar alumno", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        // Insert a new student
                        val id: Long = db.InsertarAlumno(alumno)
                        if (id > 0) {
                            Toast.makeText(requireContext(), "Alumno agregado exitosamente", Toast.LENGTH_SHORT).show()
                            limpiarCampos()
                        } else {
                            Toast.makeText(requireContext(), "Error al agregar alumno", Toast.LENGTH_SHORT).show()
                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                } finally {
                    db.close()
                }
            }
        }

        btnBuscar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isNotEmpty()) {
                try {
                    db = dbAlumnos(requireContext())
                    db.openDataBase()
                    val alumno = db.BuscarAlumno(matricula)
                    if (alumno != null) {
                        txtNombre.setText(alumno.nombre)
                        txtDomicilio.setText(alumno.domicilio)
                        txtEspecialidad.setText(alumno.especialidad)
                        txturl.setText(alumno.foto)
                        Toast.makeText(requireContext(), "Alumno encontrado", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(requireContext(), "Alumno no encontrado", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(requireContext(), "Ingrese la matrícula para buscar", Toast.LENGTH_SHORT).show()
            }
        }

        btnBorrar.setOnClickListener {
            val matricula = txtMatricula.text.toString()
            if (matricula.isNotEmpty()) {
                val builder = AlertDialog.Builder(requireContext())
                builder.setMessage("¿Está seguro de que desea eliminar este alumno?")
                    .setPositiveButton("Sí") { dialog, id ->
                        try {
                            db = dbAlumnos(requireContext())
                            db.openDataBase()
                            val result = db.BorrarAlumno(matricula)
                            if (result > 0) {
                                Toast.makeText(requireContext(), "Alumno borrado exitosamente", Toast.LENGTH_SHORT).show()
                                limpiarCampos()
                            } else {
                                Toast.makeText(requireContext(), "Error al borrar alumno", Toast.LENGTH_SHORT).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(requireContext(), "Error: ${e.message}", Toast.LENGTH_LONG).show()
                            e.printStackTrace()
                        }
                    }
                    .setNegativeButton("No") { dialog, id ->
                        dialog.dismiss()
                    }
                builder.create().show()
            } else {
                Toast.makeText(requireContext(), "Ingrese la matrícula para borrar", Toast.LENGTH_SHORT).show()
            }
        }


        btnLimpiar.setOnClickListener {
            limpiarCampos()
        }

        return view
    }


    private fun validarCampos(): Boolean {
        return when {
            txtMatricula.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(), "Ingrese la matrícula", Toast.LENGTH_SHORT).show()
                false
            }
            txtNombre.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(), "Ingrese el nombre", Toast.LENGTH_SHORT).show()
                false
            }
            txtDomicilio.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(), "Ingrese el domicilio", Toast.LENGTH_SHORT).show()
                false
            }
            txtEspecialidad.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(), "Ingrese la especialidad", Toast.LENGTH_SHORT).show()
                false
            }
            txturl.text.toString().isEmpty() -> {
                Toast.makeText(requireContext(), "Ingrese la URL de la imagen", Toast.LENGTH_SHORT).show()
                false
            }
            else -> true
        }
    }

    private fun limpiarCampos() {
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txturl.text.clear()
    }
}



